# Ansible Role: FoundryVTT

Ansible role for installing [FoundryVTT](https://foundryvtt.com/).

This role can install a single instance of FoundryVTT or multiple instances on a single machine. Each instance has it's own user, directory, and systemd service.

### Variables

#### Required

| Name | Description |
|---|---|
| `foundryvtt_website_username` | Username for foundryvtt.com |
| `foundryvtt_website_password` | Password for foundryvtt.com |
| `foundryvtt_servers` | List of dicts with server details. See Below. |

The website user/pass are used to download each version of FoundryVTT specified in `foundryvtt_servers` variable.

## Single Instance

If installing a single instance you can probably leave the defaults. This will install FoundryVTT with the below settings (defined in `defaults/main.yml`):

| Item | Value |
|---|---|
| User | `foundryvtt` |
| Group | `foundryvtt` |
| Home Dir | `/var/lib/foundryvtt` |
| Data Dir | `/var/lib/foundryvtt/data` |
| Version | `0.6.6` |
| Install dir | `/opt/foundryvtt_0.6.6` |
| Port | `30000` |
| Service Name | `foundryvtt` |

## Multiple Instances

If hosting multiple instances you'll need to duplicate the below variables.

#### Subkeys

This role configures each instance by reading the `foundryvtt_servers` variable with be below subkeys.

| Name | Default | Required | Description |
|---|---|---|---|
| `name` | N/A | Yes | Used for the name of the `systemd` service |
| `version` | N/A | Yes | Which version of FoundryVTT to install/use |
| `port` | `30000` | Yes | The port for the server will run on |
| `user` | `<name>` | No | The user to create on the system |
| `uid` | omit | No | The UID of the user |
| `group` | `<user>` | No | The group name to add the user to |
| `gid` | omit | No | The GID for the group created |
| `home` | `/var/lib/<user>` | No | The home directory for the user |
| `create_home` | `true` | No | Create the user's home dir |
| `shell` | `/sbin/nologin` | No | The default shell for the created user |
| `system` | `true` | No | User is classed as a system user |
| `data` | `/var/lib/<user>/data` | No | The datadirectory for FoundryVTT |
| `state` | `running` | No | State of the systemd service |
| `enabled` | `true` | No | If systemd service is enabled |

#### Config

Eacher server also has the `config` subkey allowing for configuration of the FoundryVTT server. For more information see the [FoundryVTT Config docs](https://foundryvtt.com/article/configuration/).

| Name | Default |
|---|---|
| `port` | `30000` |
| `upnp` | `false` |
| `fullscreen` | `false` |
| `hostname` | `null` |
| `routePrefix` | `null` |
| `sslCert` | `null` |
| `sslKey` | `null` |
| `awsConfig` | `null` |
| `dataPath` | `/var/lib/\<user\>/data` |
| `proxySSL` | `false` |
| `updateChannel` | `release` |
| `world` | `null` |

### Examples

#### Single instance

```yaml
---
foundryvtt_servers:
  - name: foundryvtt
    version: "0.6.6"
```

This will create the following:

* FoundryVTT installed
    * `/opt/foundryvtt_0.6.6`
* foundryvtt group created
    * system group
* foundryvtt user created
    * system user
    * shell: /sbin/nologin
    * home: /var/lib/foundryvtt created
    * data: /var/lib/foundryvtt/data created
* foundryvtt service enabled/created
    * port: 30000

#### Multiple instances

```yaml
---
foundryvtt_servers:
  - name: alice
    version: "0.6.5"
  - name: bob
    version: "0.6.6"
    user: bobby
    uid: 4000
    create_home: false
    shell: /bin/bash
    system: false
    group: tables
    gid: 2000
    home: /home/bob
    data: /home/bob/.foundryvtt
    state: stopped
    config:
      port: 30001
```

This will create the following:

* FoundryVTT installed
    * `/opt/foundryvtt_0.6.5`
    * `/opt/foundryvtt_0.6.6`
* alice group created
    * system group
    * gid: 2000
* alice user created
    * system user
    * uid: 4000
    * shell: `/sbin/nologin`
    * home: `/var/lib/alice` created
    * data: `/var/lib/alice/data` created
* foundryvtt-alice service enabled/started
    * version: `0.6.5`
    * port: `30000`
* bob user created
    * not system user
    * shell: `/bin/bash`
    * home: `/home/bobby` not created
    * data: `/home/bobby/.foundryvtt` created
* foundryvtt-bob service enabled/stopped
    * version: `0.6.6`
    * port: `30001`
