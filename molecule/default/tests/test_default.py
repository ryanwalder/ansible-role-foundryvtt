import distro

version = "0.6.6"

release = distro.linux_distribution()[2]


def test_nodejs(host):
    source = host.file("/etc/apt/sources.list.d/nodesource.list")
    source.exists
    source.is_file
    source.content == "deb https://deb.nodesource.com/node_14.x {} main".format(
        release
    )

    pkg = host.package("nodejs")
    pkg.is_installed


def test_foundry_user(host):
    user = host.user("foundryvtt")
    user.exists
    user.home == "/var/lib/foundryvtt"
    user.shell == "/sbin/nologin/"
    user.gid < 1000


def test_foundry_group(host):
    group = host.group("foundryvtt")
    group.exists


def test_foundry_config(host):
    config = host.file("/var/lib/foundryvtt/data/Config/options.json")

    config.exists
    config.is_file
    config.mode == "0644"
    config.user == "foundryvtt"
    config.group == "foundryvtt"

    config.content == '"port": 30000,'
    config.content == '"upnp": false,'
    config.content == '"fullscreen": false,'
    config.content == '"hostname": null,'
    config.content == '"routePrefix": null,'
    config.content == '"sslCert": null,'
    config.content == '"sslKey": null,'
    config.content == '"awsConfig": null,'
    config.content == '"dataPath": "/var/lib/foundryvtt/data",'
    config.content == '"proxySSL": false,'
    config.content == '"proxyPort": null,'
    config.content == '"updateChannel": "release",'
    config.content == '"world": null'


def test_foundry_installed(host):
    path = host.file("/opt/foundryvtt_{}".format(version))
    exe = host.file("/opt/foundryvtt_{}/foundryvtt".format(version))
    service_file = host.file("/etc/systemd/system/foundryvtt.service")

    path.exists
    path.is_directory
    path.mode == "0755"
    path.user == "root"
    path.group == "root"

    exe.exists
    exe.is_file
    exe.user == "root"
    exe.group == "root"
    exe.mode == "0755"

    service_file.exists
    service_file.user == "root"
    service_file.group == "root"
    service_file.mode == "0644"
    service_file.content == "Description=FoundryVTT Server"
    service_file.content == "User=foundryvtt"
    service_file.content == "Group=foundryvtt"
    service_file.content == "WorkingDirectory=/var/lib/foundryvtt"
    service_file.content == 'ExecStart=/usr/bin/node /opt/foundryvtt_{}/resources/app/main.js --dataPath="/var/lib/foundryvtt/data"'.format(
        version
    )
